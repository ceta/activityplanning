/**
 */
package planning;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Response Status</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see planning.PlanningPackage#getResponseStatus()
 * @model
 * @generated
 */
public final class ResponseStatus extends AbstractEnumerator {
	/**
	 * The '<em><b>ACCEPT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ACCEPT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACCEPT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ACCEPT = 1;

	/**
	 * The '<em><b>REJECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REJECT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REJECT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REJECT = 0;

	/**
	 * The '<em><b>ACCEPT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACCEPT
	 * @generated
	 * @ordered
	 */
	public static final ResponseStatus ACCEPT_LITERAL = new ResponseStatus(ACCEPT, "ACCEPT", "ACCEPT");

	/**
	 * The '<em><b>REJECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REJECT
	 * @generated
	 * @ordered
	 */
	public static final ResponseStatus REJECT_LITERAL = new ResponseStatus(REJECT, "REJECT", "REJECT");

	/**
	 * An array of all the '<em><b>Response Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ResponseStatus[] VALUES_ARRAY =
		new ResponseStatus[] {
			ACCEPT_LITERAL,
			REJECT_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Response Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Response Status</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResponseStatus get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ResponseStatus result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Response Status</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResponseStatus getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ResponseStatus result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Response Status</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResponseStatus get(int value) {
		switch (value) {
			case ACCEPT: return ACCEPT_LITERAL;
			case REJECT: return REJECT_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ResponseStatus(int value, String name, String literal) {
		super(value, name, literal);
	}

} //ResponseStatus
