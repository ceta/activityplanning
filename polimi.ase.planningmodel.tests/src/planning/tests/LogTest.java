/**
 */
package planning.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import planning.Log;
import planning.PlanningFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Log</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LogTest extends TestCase {

	/**
	 * The fixture for this Log test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Log fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LogTest.class);
	}

	/**
	 * Constructs a new Log test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Log test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Log fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Log test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Log getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(PlanningFactory.eINSTANCE.createLog());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LogTest
