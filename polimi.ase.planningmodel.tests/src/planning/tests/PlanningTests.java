/**
 */
package planning.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>planning</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlanningTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new PlanningTests("planning Tests");
		suite.addTestSuite(ProjectTest.class);
		suite.addTestSuite(PersonTest.class);
		suite.addTestSuite(TaskTest.class);
		suite.addTestSuite(ResponseTest.class);
		suite.addTestSuite(NotificationTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlanningTests(String name) {
		super(name);
	}

} //PlanningTests
