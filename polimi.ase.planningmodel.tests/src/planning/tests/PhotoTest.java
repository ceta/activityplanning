/**
 */
package planning.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import planning.Photo;
import planning.PlanningFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PhotoTest extends TestCase {

	/**
	 * The fixture for this Photo test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Photo fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PhotoTest.class);
	}

	/**
	 * Constructs a new Photo test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhotoTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Photo test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Photo fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Photo test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Photo getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(PlanningFactory.eINSTANCE.createPhoto());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PhotoTest
